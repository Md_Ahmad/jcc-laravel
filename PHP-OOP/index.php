<?php
// Mamad Ahmad
function xo($str) {
    $hitung1 = substr_count($str, "x");
    $hitung2 = substr_count($str, "o");
    if($hitung1 == $hitung2){
        echo $str . " : Benar <br>";
    }else {
        echo $str . " : Salah <br>";
    }
    
}
    
    // Test Cases
    echo xo('xoxoxo'); // "Benar"
    echo xo('oxooxo'); // "Salah"
    echo xo('oxx'); // "Salah"
    echo xo('xxooox'); // "Benar"
    echo xo('xoxooxxo'); // "Benar"
?>